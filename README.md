# Attributes

* cts-container
	* cts-type
	* cts-auto-init (default: true)
* cts-block
	* cts-item
		* cts-page (pagination only)

# Blocks

## Filter

### Items

* limit
* search

## Table
### Items
* data

## Pagination
### Items
* list
* first
* previous
* page
	* cts-page (for tag that contains page number)
* next
* last

### Types

* static (default)
* numbers
* simple
* simple_number
* full
* full_number

### Examples

___
> All items (except **previous** and **next**) are not required
___

```html
<div class="col-md-12" cts-block="pagination" cts-type="[PAGINATION TYPE]">
	<nav aria-label="Page navigation" class="text-center">
    	<ul class="pagination" cts-item="list">
        	<li cts-item="first">
        		<a href="#"><span aria-hidden="true">First</span></a>
        	</li>
            <li cts-item="previous">
                <a href="#"><span aria-hidden="true">&laquo;</span></a>
            </li>
            <li cts-item="page"><a href="#" cts-page="number">2</a></li>
            <li cts-item="next">
                <a href="#"><span aria-hidden="true">&raquo;</span></a>
            </li>
            <li cts-item="last">
                <a href="#"><span aria-hidden="true">Last</span></a>
            </li>
		</ul>
	</nav>
</div>
```

#### Static
Will allow you to define your style for pagination block. Library won't parse this block and won't set events for page changing.

#### Numbers
![Numbers type](/readme_resources/images/numbers.jpg)

#### Simple
![Simple type](/readme_resources/images/simple.jpg)

#### Simple number
![Simple number type](/readme_resources/images/simple_number.jpg)

#### Full
![Full type](/readme_resources/images/full.jpg)

#### Full number
![Full number type](/readme_resources/images/full_number.jpg)