"use strict";
if (!CTS_SF) {
    var CTS_SF = {};
}

/**
 * @param {(Node|HTMLElement)} element
 * @param {string} attribute
 * @returns {{}}
 * @desc Returns found elements by attribute name
 */
CTS_SF.getElementsByAttribute = function(element, attribute) {
    element = element || document;

    var foundElements = element.getElementsByTagName('*');
    var matchingElements = {};

    for (var i = 0; i < foundElements.length; i++)
    {
        if (foundElements[i].getAttribute(attribute) !== '') {
            matchingElements[foundElements[i].getAttribute(attribute)] = foundElements[i];
        }
    }
    return matchingElements;
};

/**
 * @param {(object|string)} object
 * @returns {*}
 * @desc Returns cloned object
 */
CTS_SF.clone = function (object) {
    if (object !== null && typeof object === "object") {
        var copy = object.length !== undefined ? [] : {};
        for (var attribute in object) {
            if (object.hasOwnProperty(attribute)) {
                copy[attribute] = object[attribute];
            }
        }
        return copy;
    }
    return object;
};