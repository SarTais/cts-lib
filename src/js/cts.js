
function CTS() {
    var self = this;

    /* Properties block */
    self.containers = null;
    /* ~~~~~~~~~~~~~~~~ */

    self.init = function() {
        self.containers = CTS_SF.getElementsByAttribute(null, "cts-container");

        for(var container in self.containers) {
            if (self.containers.hasOwnProperty(container) && self.containers[container].getAttribute('cts-auto-init') !== "false") {
                self.containers[container] = new CTSContainer(self.containers[container], {type: self.containers[container].getAttribute('cts-type')});
            }
        }
    };

    self.init();
}

document.addEventListener('DOMContentLoaded', function() {
    new CTS();
});