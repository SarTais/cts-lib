function CTSContainer(cts_container, options) {
    var self = this;

    /* Constants block */
    var _STATIC_ = 'static';
    /* ~~~~~~~~~~~~~~~ */

    /* Properties block */
    self.data = null;
    self.blocks = {
        filter: null,
        table: null,//TODO Add table data
        pagination: null
    };
    /* ~~~~~~~~~~~~~~~~ */

    self.init = function(cts_container, options) {
        self.data = new CTSData(options);
        self.setEventListeners();
        
        var cts_blocks = CTS_SF.getElementsByAttribute(cts_container, "cts-block");

        if (cts_blocks.hasOwnProperty('filter')) {
            var elements = CTS_SF.getElementsByAttribute(cts_blocks.filter, 'cts-item');
            if (elements) {
                if (elements.hasOwnProperty('limit')) {
                    self.data.setLimitValue(elements.limit);
                }

                if (elements.hasOwnProperty('search')) {
                    self.data.setSearchItem(elements.search);
                }
            }

            self.blocks.filter = {
                node: cts_blocks.filter,
                elements: elements
            };
        }

        if (cts_blocks.hasOwnProperty('pagination')) {
            var paginationType = cts_blocks.pagination.getAttribute('cts-type') || _STATIC_;
            if (paginationType !== _STATIC_) {
                self.blocks.pagination = new CTSPagination(cts_blocks.pagination, {data: self.data, type: paginationType});
            }
        }

        if (cts_blocks.hasOwnProperty('table')) {
            self.blocks.table = new CTSTable(cts_blocks.table, {data: self.data});
        }
    };

    self.setEventListeners = function() {
        document.addEventListener('cts_data_updated', self.renderBlocks, false);
    };

    self.renderBlocks = function(event) {
        event.preventDefault();
        for (var block in self.blocks) {
            if (self.blocks.hasOwnProperty(block) && self.blocks[block] !== null && self.blocks[block].render !== undefined) {
                self.blocks[block].render();
            }
        }
    };

    self.init(cts_container, options);
}