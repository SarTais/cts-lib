function CTSPagination(cts_block, options) {
    var self = this;

    /* Constants block */
    var _NUMBERS_ = 'numbers';
    var _SIMPLE_ = 'simple';
    var _SIMPLE_NUMBER_ = 'simple_number';
    var _FULL_ = 'full';
    var _FULL_NUMBER_ = 'full_number';
    /* ~~~~~~~~~~~~~~~ */

    /* Properties block */
    self.container = null;
    self.data = null;
    self.type = null;
    /* ~~~~~~~~~~~~~~~~ */

    /**
     *
     * @param {HTMLElement} cts_block - Contains element with cts-pagination attribute
     * @param {object} options - Contains library options
     * @desc CTSPagination's module constructor
     */
    self.init = function(cts_block, options) {
        self.type = options.type;
        self.data = options.data;
        self.container = {
            node: cts_block,
            listOfPages: null,
            elements: null
        };

        var elements = CTS_SF.getElementsByAttribute(self.container.node, 'cts-item');
        if (elements && elements.list !== undefined) {
            self.container.listOfPages = elements.list;

            if (elements.first !== undefined) {
                self.setPageLinkEvent(elements.first, function () {
                    return 1;
                });
            }

            if (elements.previous !== undefined) {
                self.setPageLinkEvent(elements.previous, function () {
                    return self.data.getCurrentPage() - 1;
                });
            }

            if (elements.next !== undefined) {
                self.setPageLinkEvent(elements.next, function () {
                    return self.data.getCurrentPage() + 1;
                });
            }

            if (elements.last !== undefined) {
                self.setPageLinkEvent(elements.last, function () {
                    var page = Math.round(self.data.getCountOfDisplayedRows() / self.data.getLimitValue());
                    return page * self.data.getLimitValue() < self.data.getCountOfDisplayedRows() ? page + 1 : page;
                });
            }
        }
        self.container.elements = CTS_SF.clone(elements);
    };

    /* Pagination */

    /**
     * @desc Renders pagination block
     */
    self.render = function() {
        /**
         * @param {HTMLElement} self.container.elements.list - Contains template of links container.
         */
        if (self.container !== undefined && self.container.elements && self.container.elements.list !== undefined) {
            var linkContainer = self.container.elements.list;
            linkContainer.innerHTML = '';
            switch (self.type) {
                case _NUMBERS_:
                    self.renderNumbersLinks(linkContainer, self.container.elements, true);
                    break;
                case _SIMPLE_:
                    self.renderNavigationLinks(linkContainer, self.container.elements, false);
                    break;
                case _SIMPLE_NUMBER_:
                    self.renderAllLinks(linkContainer, self.container.elements, false, false);
                    break;
                case _FULL_:
                    self.renderAllLinks(linkContainer, self.container.elements, true, true);
                    break;
                case _FULL_NUMBER_:
                    self.renderAllLinks(linkContainer, self.container.elements, false, true);
                    break;
                default:
                    self.renderAllLinks(linkContainer, self.container.elements, true, false);
                    break;
            }
        }
    };

    /**
     *
     * @param {HTMLElement} linkContainer - Contains parent element for pagination buttons
     * @param {HTMLElement} elements - Contains templates of navigation elements
     * @param {HTMLElement} elements.page - Contains template for button with page number
     * @param {boolean} isFull - Should this function draw all page numbers or current only
     * @returns {boolean}
     * @desc Renders number links and returns result
     */
    self.renderNumbersLinks = function(linkContainer, elements, isFull) {
        if (elements.page !== undefined) {
            if (!isFull) {
                linkContainer.appendChild(self.preparePageLink(elements.page, self.data.getCurrentPage()));
            } else {
                var i, maxPage = Math.round(self.data.getCountOfDisplayedRows() / self.data.getLimitValue());
                maxPage = maxPage * self.data.getLimitValue() < self.data.getCountOfDisplayedRows() ? maxPage + 1 : maxPage;
                if (maxPage < 2) {
                    return self.renderNumbersLinks(linkContainer, elements, false);
                }

                if (maxPage < 9) {
                    for (i = 1; i <= maxPage; ++i) {
                        linkContainer.appendChild(self.preparePageLink(elements.page, i));
                    }
                } else {
                    var from, to;

                    if (self.data.getCurrentPage() > 4) {
                        linkContainer.appendChild(self.preparePageLink(elements.page, 1));
                        linkContainer.appendChild(self.preparePageLink(elements.page, '...'));
                    }

                    var offsetCurrent = Math.abs(self.data.getCurrentPage() - 5);
                    var offsetMaxPage = Math.abs(self.data.getCurrentPage() - maxPage);
                    if (self.data.getCurrentPage() <= 5 && offsetCurrent > 0 && offsetCurrent <= 4) {
                        from = self.data.getCurrentPage() - (4 - offsetCurrent);
                        to = from + 4;
                    } else if (self.data.getCurrentPage() > 5 && offsetMaxPage >= 0 && offsetMaxPage <= 4) {
                        from = self.data.getCurrentPage() - (4 - offsetMaxPage);
                        to = maxPage;
                    } else {
                        from = self.data.getCurrentPage() - 1;
                        to = from + 2;
                    }

                    for(i = from; i <= to; i++) {
                        linkContainer.appendChild(self.preparePageLink(elements.page, i));
                    }

                    if (from + 5 < maxPage) {
                        linkContainer.appendChild(self.preparePageLink(elements.page, '...'));
                        linkContainer.appendChild(self.preparePageLink(elements.page, maxPage));
                    }
                }
            }
            return true;
        }
        return false;
    };

    /**
     * @param {HTMLElement} linkContainer - Contains parent element for pagination buttons
     * @param {object} elements - Contains templates of navigation elements
     * @param {HTMLElement} elements.first - Contains template of first page button
     * @param {HTMLElement} elements.previous - Contains template of previous page button
     * @param {HTMLElement} elements.next - Contains template of next page button
     * @param {HTMLElement} elements.last - Contains template of last page button
     * @param {boolean} isFull - Should this function draw all types of buttons or next/previous only
     * @returns {boolean}
     * @desc Renders navigation links and returns result
     */
    self.renderNavigationLinks = function(linkContainer, elements, isFull) {
        if (elements.previous !== undefined && elements.next !== undefined) {

            linkContainer.insertBefore(elements.previous, linkContainer.firstChild);
            linkContainer.appendChild(elements.next);

            if (isFull && elements.hasOwnProperty('first') && elements.hasOwnProperty('last')) {
                linkContainer.insertBefore(elements.first, linkContainer.firstChild);
                linkContainer.appendChild(elements.last);
            }

            self.changeActiveStatusForNextLinks(elements);
            self.changeActiveStatusForPreviousLinks(elements);

            return true;
        }
        return false;
    };

    /**
     * @param {HTMLElement} linkContainer - Contains parent element for pagination buttons
     * @param elements - Contains templates of navigation elements
     * @param isFullNumbers - - Should this function draw all page numbers or current only
     * @param isFullNavigation - Should this function draw all types of buttons or next/previous only
     */
    self.renderAllLinks = function(linkContainer, elements, isFullNumbers, isFullNavigation) {
        self.renderNumbersLinks(linkContainer, elements, isFullNumbers);
        self.renderNavigationLinks(linkContainer, elements, isFullNavigation);
    };

    /**
     * @param {HTMLElement} element - Contains template of number button
     * @param {(number|string)} page - Contains page number or '...'
     * @returns {Node}
     * @desc Renders number link using its template and returns it
     */
    self.preparePageLink = function(element, page) {
        /**
         * @desc Copied page's link template
         * @type {Node}
         */
        var pageLink = element.cloneNode(true);

        /**
         * @param numberContainer.number - Contains element that should contain page number
         */
        var numberContainer = CTS_SF.getElementsByAttribute(pageLink, 'cts-page').number;
        numberContainer.innerHTML = page;

        if (page !== '...') {
            self.changeActiveStatus(pageLink, page === self.data.getCurrentPage() ? true : null);
            self.setPageLinkEvent(pageLink, function() { return page; });
        }

        return pageLink;
    };

    /**
     * @param {Node} element - Contains pagination links that should contained passed event
     * @param {function} page - Contains click event for passed link
     * @desc Sets click event to passed link
     */
    self.setPageLinkEvent = function(element, page) {
        element.addEventListener('click', function (event) {
            event.preventDefault();
            self.data.changeCurrentPage(page());
        });
    };

    /**
     * @param {object} elements - Contains templates of navigation elements
     * @desc Sets active status for first/last links
     */
    self.changeActiveStatusForNextLinks = function(elements) {
        var names = ['next', 'last'];
        for (var name in names) {
            if (names.hasOwnProperty(name) && elements.hasOwnProperty(names[name])) {
                self.changeActiveStatus(elements[names[name]], self.data.getLimitValue() === -1 || self.data.getCurrentPage() * self.data.getLimitValue() >= self.data.getCountOfDisplayedRows() ? false : null);
            }
        }
    };

    /**
     * @param {object} elements
     * @desc Sets active status for previous/next links
     */
    self.changeActiveStatusForPreviousLinks = function(elements) {
        var names = ['previous', 'first'];
        for (var name in names) {
            if (names.hasOwnProperty(name) && elements.hasOwnProperty(names[name])) {
                self.changeActiveStatus(elements[names[name]], self.data.getCurrentPage() === 1 ? false : null);
            }
        }
    };

    /**
     *
     * @param {Node} element
     * @param status - Contains link status active/disabled
     * @desc Sets status for number links
     */
    self.changeActiveStatus = function(element, status) {
        if (status === undefined || status === null) {
            element.classList.remove('active', 'disabled');
        } else {
            element.classList.add(status === true ? 'active' : 'disabled');
        }
    };
    /* ~~~~~~~~~~~~~~~~~~~ */

    self.init(cts_block, options);
}
