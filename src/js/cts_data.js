function CTSData(options) {
    var self = this;

    /* START CONSTANTS BLOCK */
    var _STATIC_TYPE_ = 'static';
    var _DYNAMIC_TYPE_ = 'dynamic';
    /* END CONSTANTS BLOCK*/

    /* Properties block */
    self.type = null;
    self.currentPage = null;
    self.rowsPerPage = null;
    self.data = null;
    self.displayedData = null;

    self.search = {};
    /* ~~~~~~~~~~~~~~~~ */

    self.init = function(options) {
        self.setType(options.type);
        self.setCurrentPage(options.currentPage);
        self.setLimitValue(options.rowsPerPage);
        self.setTableData(options.tableData);
        self.setDisplayedData(options.displayedData);
    };

    self.getType = function() { return self.type; };
    self.setType = function(type) { self.type = type || _STATIC_TYPE_; };
    self.isStaticType = function() { return self.type === _STATIC_TYPE_; };
    self.isDynamicType = function() { return self.type === _DYNAMIC_TYPE_; };

    self.getCurrentPage = function() { return self.currentPage; };
    self.setCurrentPage = function(page) {
        self.currentPage = page || 1;
        self.currentPage = self.getCurrentPage() < 1 ? 1 : self.getCurrentPage();
    };
    self.changeCurrentPage = function(page) {
        self.setCurrentPage(page);
        if (self.displayedData !== null) {
            self.setCurrentPage((self.getCurrentPage() - 1) * self.getLimitValue() >= self.displayedData.length
                                ? self.getCurrentPage() - 1 : self.getCurrentPage());
        } else {
            //TODO Get new page and count of pages from ajax
        }

        if (self.getCurrentPage() === page) {
            var event = new CustomEvent('cts_data_updated', { });
            document.dispatchEvent(event);
        }
    };

    self.getLimitValue = function() {
        var value = -1;

        if (self.rowsPerPage !== null) {
            if (self.rowsPerPage instanceof Element) {
                switch (self.rowsPerPage.tagName.toLowerCase()) {
                    case 'select':
                        value = self.rowsPerPage.options[self.rowsPerPage.selectedIndex].value;
                        break;
                    default:
                        value = self.rowsPerPage.value;
                        break;
                }
            }
            else { value = self.rowsPerPage; }
        }

        return +value;
    };
    self.setLimitValue = function(val) {
        self.rowsPerPage = val || 5;
        if (self.rowsPerPage instanceof Element) {
            self.rowsPerPage.addEventListener('change', self.onLimitChanged);
        }
    };
    self.onLimitChanged = function() {
        if (self.data !== null) {
            self.changeCurrentPage(1);
        } else {
            //TODO Send ajax request after changing limit
        }
    };

    self.rebuildTableBody = function () {};
    self.rebuildPagination = function() {};

    self.setTableData = function(data) { self.data = data || null; };
    self.getTableData = function () { return self.data; };

    self.getDisplayedData = function() { return self.displayedData; };
    self.setDisplayedData = function(data) { self.displayedData = data || self.getTableData(); };

    self.getCountOfDisplayedRows = function() { return self.displayedData ? self.displayedData.length : 0; };

    self.setSearchItem = function(item) {
        self.search.value = item;
        self.search.value.addEventListener('keypress', function(event) {
            if (event.keyCode === 13) {
                event.preventDefault();
                self.getSearchResult();
            }
        });

        var buttonId = item.getAttribute('cts-search-button');
        if (buttonId !== null) {
            self.search.button = document.getElementById(buttonId);
            if (self.search.button !== null) {
                self.search.button.addEventListener('click', self.getSearchResult);
            }
        } else { self.search.button = buttonId; }
    };
    self.getSearchResult = function() {
        if (self.search.value instanceof HTMLInputElement) {
            var search = self.search.value.value.toLowerCase();
            if (self.type === _STATIC_TYPE_) {
                var displayedData = [];
                var tableData = self.getTableData();
                for (var i in tableData) {
                    if (tableData.hasOwnProperty(i)) {
                        for (var j = 0; j < tableData[i].cells.length; j++) {
                            if (tableData[i].cells[j].innerText.toLowerCase().indexOf(search) !== -1) {
                                displayedData.push(tableData[i]);
                                break;
                            }
                        }
                    }
                }
                self.setDisplayedData(displayedData);
                self.changeCurrentPage(1);
            } else {
                //TODO Ajax request to rebuild table
            }
        }
        else { console.log('Search item must be an HTMLInputElement!'); }
    };

    self.init(options);
}