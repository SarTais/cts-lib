function CTSTable(cts_block, options) {
    var self = this;

    /* Properties block */
    self.container = null;
    self.data = null;
    self.type = null;
    /* ~~~~~~~~~~~~~~~~ */

    /**
     * @param {HTMLElement} cts_block - Contains element with cts-table attribute
     * @param {object} options - Contains library options
     * @desc CTSTable's module constructor
     */
    self.init = function(cts_block, options) {
        self.data = options.data;
        self.container = {
            node: cts_block,
            table: {
                node: null,
                tbody: null,
                loader: null
            }
        };

        var elements = CTS_SF.getElementsByAttribute(self.container.node, 'cts-item');
        if (elements && elements.data !== undefined) {
            self.container.table.node = elements.data;
            self.container.table.loader = self.createLoader(null);
            self.container.node.insertBefore(self.container.table.loader, self.container.table.node);

            self.setTableBody();

            if (self.data.isStaticType()) {
                self.data.setTableData(self.container.table.tbody ? CTS_SF.clone(self.container.table.tbody.getElementsByTagName('tr')) : null);
                self.data.setDisplayedData(self.data.getTableData());
            }
        }
        self.data.changeCurrentPage(1);
    };

    /**
     * @param {(null|string)} image - Contains path to the loader image
     * @returns {Element}
     * @desc Renders loader animation block for table body
     */
    self.createLoader = function(image) {
        var div = document.createElement('div');
        div.className += 'cts_loader';
        div.style.height = self.container.node.clientHeight + 'px';
        div.style.width = self.container.node.clientWidth + 'px';
        div.style.display = 'none';

        var img = new Image();
        img.src = image ? image : 'images/loading.gif';//TODO Added loader to settings
        img.width = 50;

        div.appendChild(img);
        return div;
    };

    /**
     * @desc Finds and sets tbody element in containers variable
     */
    self.setTableBody = function() {
        if (self.container.table.node && self.container.table.node instanceof Element) {
            self.container.table.tbody = self.container.table.node.getElementsByTagName('tbody');
            if (self.container.table.tbody && self.container.table.tbody.length > 0) {
                self.container.table.tbody = self.container.table.tbody[0];
            }
        } else { self.container.table.tbody = null; }
    };

    /**
     * @desc Renders table's body data
     */
    self.render = function() {
        var limit = self.data.getLimitValue();
        if (self.data.isStaticType()) {
            if (self.data.getDisplayedData() !== null) {
                if (self.container.table.tbody === null) {
                    self.setTableBody();
                }

                if (self.container.table.tbody !== null) {
                    self.container.table.tbody.innerHTML = '';

                    var startItem = (self.data.getCurrentPage() - 1) * limit;
                    var endItem = limit !== -1 ? startItem + limit : self.data.getDisplayedData().length;

                    for (var i = startItem; i < endItem; i++) {
                        if (i >= self.data.getDisplayedData().length) {
                            break;
                        }

                        self.container.table.tbody.appendChild(self.data.getDisplayedData()[i]);
                    }
                }
            }
        } else {
            //TODO Ajax tbody load
        }
    };

    self.init(cts_block, options);
}