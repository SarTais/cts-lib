<html>
    <head>
        <title>CTS</title>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="css/cts.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
        <script type="text/javascript" src="js/cts.js"></script>
    </head>
    <body>
        <div class="col-md-8 col-md-offset-2 block" id="testTable" cts-container="testTable" cts-id="testTable" cts-type="static">
            <div class="col-md-12" cts-block="filter">
                <div class="row col-12">
                    <form class="form-inline col-12">
                        <div class="col-6">
                            <div class="form-group">
                                <select class="form-control items-per-page" title="" cts-item="limit">
                                    <option value="2" selected>2</option>
                                    <option value="5">5</option>
                                    <option value="10">10</option>
                                    <option value="15">15</option>
                                    <option value="20">20</option>
                                    <option value="25">25</option>
                                    <option value="-1">All</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                <input class="form-control" id="search_content" type="text" name="search" placeholder="Search..." value="" cts-item="search" cts-search-button="search_content_btn">
                                &nbsp;&nbsp;<button type="button" class="btn btn-primary"  id="search_content_btn">Search</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-12" cts-block="table">
                <table class="table table-bordered table-hover" cts-item="data">
                    <thead>
                        <tr>
                            <th>№</th>
                            <th>Name</th>
                            <th>Value</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr><td>1</td><td>1 Name</td><td>1 Value</td></tr>
                        <tr><td>2</td><td>2 Name</td><td>2 Value</td></tr>
                        <tr><td>3</td><td>3 Name</td><td>3 Value</td></tr>
                        <tr><td>4</td><td>4 Name</td><td>4 Value</td></tr>
                        <tr><td>5</td><td>5 Name</td><td>5 Value</td></tr>
                        <tr><td>6</td><td>6 Name</td><td>6 Value</td></tr>
                        <tr><td>7</td><td>7 Name</td><td>7 Value</td></tr>
                        <tr><td>8</td><td>8 Name</td><td>8 Value</td></tr>
                        <tr><td>9</td><td>9 Name</td><td>9 Value</td></tr>
                        <tr><td>10</td><td>10 Name</td><td>10 Value</td></tr>
                        <tr><td>11</td><td>11 Name</td><td>11 Value</td></tr>
                        <tr><td>12</td><td>12 Name</td><td>12 Value</td></tr>
                        <tr><td>13</td><td>13 Name</td><td>13 Value</td></tr>
                        <tr><td>14</td><td>14 Name</td><td>14 Value</td></tr>

                        <tr><td>15</td><td>15 Name</td><td>15 Value</td></tr>
                        <tr><td>16</td><td>16 Name</td><td>16 Value</td></tr>
                        <tr><td>17</td><td>17 Name</td><td>17 Value</td></tr>
                        <tr><td>18</td><td>18 Name</td><td>18 Value</td></tr>
                        <tr><td>19</td><td>19 Name</td><td>19 Value</td></tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-12" cts-block="pagination" cts-type="full">
                <nav aria-label="Page navigation" class="text-center">
                    <ul class="pagination" cts-item="list">
                        <li cts-item="first">
                            <a href="#">
                                <span aria-hidden="true">First</span>
                            </a>
                        </li>
                        <li cts-item="previous">
                            <a href="#">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <li cts-item="page"><a href="#" cts-page="number">2</a></li>
                        <li cts-item="next">
                            <a href="#">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                        <li cts-item="last">
                            <a href="#">
                                <span aria-hidden="true">Last</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </body>
</html>