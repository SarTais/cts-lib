module.exports = function(grunt) {
    grunt.initConfig({

        jshint: {
            beforeconcat: ['src/js/cts.js', 'src/js/cts_pagination.js', 'src/js/cts_sort.js'],
            afterconcat: ['examples/js/cts.js']
        },

        concat: {
            dist: {
                files: [
                    {
                        src: ['src/js/cts_secondary_functions.js', 'src/js/cts_data.js', 'src/js/cts.js', 'src/js/cts_container.js', 'src/js/cts_table.js', 'src/js/cts_pagination.js', 'src/js/cts_sort.js'],
                        dest: 'examples/js/cts.js'
                    },
                    {
                        src: ['src/css/cts.css'],
                        dest: 'examples/css/cts.css'
                    }
                ]
            }
        },

        symlink: {
            options: {
                overwrite: false,
                force: false
            },
            explicit: {
                files: [
                    {src: 'node_modules/bootstrap/dist/css/',   dest: 'examples/css/bootstrap/'   },
                    {src: 'node_modules/bootstrap/dist/js/',    dest: 'examples/js/bootstrap/'    },
                    {src: 'node_modules/bootstrap/dist/fonts/', dest: 'examples/fonts/bootstrap/' }
                ]
            }
        }

    });

    grunt.loadNpmTasks('grunt-contrib-symlink');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');

    grunt.registerTask('default', ['jshint', 'concat']);
};